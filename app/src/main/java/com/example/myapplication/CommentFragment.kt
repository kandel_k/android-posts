package com.example.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.observe
import androidx.navigation.fragment.navArgs
import com.example.myapplication.adapter.MyCommentRecyclerViewAdapter
import com.example.myapplication.data.Comment
import com.example.myapplication.utils.InjectorUtils
import com.example.myapplication.viewModel.PostDetailViewModel

/**
 * A fragment representing a list of Items.
 */
class CommentFragment() : Fragment() {

    private var columnCount = 1
    private lateinit var viewAdapter: MyCommentRecyclerViewAdapter

//    private val args: PostDetailFragmentArgs by navArgs()
//    private val viewModel: PostDetailViewModel by viewModels {
//        InjectorUtils.providePostDetailViewModelFactory(requireContext(), args.postId)
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_comment_list, container, false)
        viewAdapter = MyCommentRecyclerViewAdapter()

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                adapter = viewAdapter
            }
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState);
//        viewModel.comments.observe(viewLifecycleOwner) {
//            viewAdapter.setItems(it)
//        }
    }

    companion object {

        const val ARG_COLUMN_COUNT = "column-count"

        @JvmStatic
        fun newInstance(columnCount: Int) =
            CommentFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}