package com.example.myapplication.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface PostDao {
    @Query("SELECT * FROM posts ORDER BY id")
    fun getPosts(): LiveData<List<Post>>

    @Query("SELECT * FROM post_details WHERE id = :postId")
    fun getLivePostWithDetails(postId: String): LiveData<PostWithUsersAndComments>

    @Query("SELECT * FROM post_details")
    fun getPostsWithDetails(): List<PostWithUsersAndComments>

    @Query("SELECT * FROM comments WHERE postId = :postId")
    fun getComments(postId: String): LiveData<List<Comment>>

    @Query("DELETE FROM posts")
    fun clearPosts()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(posts: List<Post>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(user: User)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertComments(comments: List<Comment>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPostDetails(post: DetailPost)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPostWithUserAndComments(post: DetailPost, user: User, comments: List<Comment>)
}