package com.example.myapplication.data

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation

@Entity(tableName = "post_details")
class DetailPost(
    @PrimaryKey val id: String,
    val title: String,
    val body: String,
    val userId: String
)