package com.example.myapplication.data

import androidx.lifecycle.LiveData
import com.example.myapplication.api.WebService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException
import java.lang.NullPointerException

class PostRepository private constructor(
    private val webService: WebService,
    private val postDao: PostDao
) {

    fun getPosts(): LiveData<List<Post>> {
        GlobalScope.launch {
            refreshPosts()
        }
        return postDao.getPosts()
    }

    private suspend fun refreshPosts() {
        try {
            val posts = webService.getPosts().execute().body()!!

            posts.forEach {
                val user = webService.getUser(it.userId.toString()).execute().body()!!
                it.name = user.name
            }

            postDao.insertAll(posts)
        } catch (e: IOException) {
            // TODO print locally saved data to user
            postDao.clearPosts()
            postDao.insertAll(postDao.getPostsWithDetails().map { it.toPost() })
        } catch (e: NullPointerException) {
        }
    }

    private fun PostWithUsersAndComments.toPost() = Post(
        id = post.id,
        body = post.body,
        userId = post.userId.toInt(),
        title = post.title,
        name = user[0].name
    )

    fun getPostsWithDetails(postId: String): LiveData<PostWithUsersAndComments> {
        GlobalScope.launch {
            loadAndSavePostDetails(postId)
        }
        return postDao.getLivePostWithDetails(postId)
    }

    fun getComments(postId: String): LiveData<List<Comment>> {
        return postDao.getComments(postId)
    }

    private suspend fun loadAndSavePostDetails(postId: String) {
        try {
            val post: DetailPost = webService.getPost(postId).execute().body()!!
            val user: User = webService.getUser(post.userId).execute().body()!!
            val comments: List<Comment> = webService.getComments(postId).execute().body()!!

            postDao.insertPostWithUserAndComments(post, user, comments)
        } catch (e: IOException) {
        }
        catch (e: NullPointerException) {
        }
    }

    companion object {

        @Volatile
        private var instance: PostRepository? = null

        fun getInstance(webService: WebService, postDao: PostDao) =
            instance ?: synchronized(this) {
                instance ?: PostRepository(webService, postDao).also { instance = it }
            }
    }
}