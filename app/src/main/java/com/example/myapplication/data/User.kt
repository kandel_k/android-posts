package com.example.myapplication.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity (tableName = "users")
data class User(
    @PrimaryKey val id: String,
    val email: String,
    val username: String,
    val name: String,
    val phone: String
)