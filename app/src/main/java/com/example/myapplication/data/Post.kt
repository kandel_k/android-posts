package com.example.myapplication.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "posts")
data class Post (
    @PrimaryKey val id: String,
    val title: String,
    val body: String,
    val userId: Int,
    var name: String
) {

    override fun toString(): String = title
}