package com.example.myapplication.data

import androidx.room.Embedded
import androidx.room.Relation

data class PostWithUsersAndComments(
    @Embedded val post: DetailPost,
    @Relation(
        parentColumn = "userId",
        entityColumn = "id"
    )
    val user: List<User>,
    @Relation(
        parentColumn = "id",
        entityColumn = "postId"
    )
    val comments: List<Comment>
)