package com.example.myapplication.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity (tableName = "comments")
data class Comment(
    @PrimaryKey val id: String,
    val name: String,
    val postId: String,
    val email: String,
    val body: String
)