package com.example.myapplication.utils

import android.content.Context
import com.example.myapplication.api.WebService
import com.example.myapplication.data.PostDatabase
import com.example.myapplication.data.PostRepository
import com.example.myapplication.viewModel.PostDetailViewModel
import com.example.myapplication.viewModel.PostDetailViewModelFactory
import com.example.myapplication.viewModel.PostsViewModelFactory

object InjectorUtils {
    fun providePostsViewModelFactory(context: Context): PostsViewModelFactory {
        val database = PostDatabase.getInstance(context)
        val repository = PostRepository.getInstance(WebService.create(), database.postDao());
        return PostsViewModelFactory(repository)
    }

    fun providePostDetailViewModelFactory(context: Context, postId: String): PostDetailViewModelFactory {
        val database = PostDatabase.getInstance(context)
        val repository = PostRepository.getInstance(WebService.create(), database.postDao());
        return PostDetailViewModelFactory(repository, postId)
    }
}