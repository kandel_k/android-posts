package com.example.myapplication.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.data.PostRepository

class PostDetailViewModelFactory (
    private val postRepository: PostRepository,
    private val postId: String
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PostDetailViewModel(
            postRepository,
            postId
        ) as T
    }
}