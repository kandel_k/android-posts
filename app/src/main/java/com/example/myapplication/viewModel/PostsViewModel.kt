package com.example.myapplication.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.data.Post
import com.example.myapplication.data.PostRepository

class PostsViewModel constructor(
    postRepository: PostRepository
) : ViewModel() {

    val posts: LiveData<List<Post>> = postRepository.getPosts();
}