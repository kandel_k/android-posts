package com.example.myapplication.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.data.Comment
import com.example.myapplication.data.DetailPost
import com.example.myapplication.data.PostRepository
import com.example.myapplication.data.PostWithUsersAndComments
import javax.inject.Singleton

@Singleton
class PostDetailViewModel(
    postRepository: PostRepository,
    postId: String
) : ViewModel() {

    val postWithDetails: LiveData<PostWithUsersAndComments> = postRepository.getPostsWithDetails(postId)
    val comments: LiveData<List<Comment>> = postRepository.getComments(postId)
}