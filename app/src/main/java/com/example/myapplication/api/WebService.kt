package com.example.myapplication.api

import com.example.myapplication.data.Comment
import com.example.myapplication.data.DetailPost
import com.example.myapplication.data.Post
import com.example.myapplication.data.User
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface WebService {

    @GET("posts")
    fun getPosts(): Call<List<Post>>

    @GET("users/{userId}")
    fun getUser(@Path("userId") userId: String): Call<User>

    @GET("posts/{postId}/comments")
    fun getComments(@Path("postId") postId: String): Call<List<Comment>>

    @GET("posts/{postId}")
    fun getPost(@Path("postId") postId: String): Call<DetailPost>

    companion object {
        private const val BASE_URL = "https://jsonplaceholder.typicode.com/"

        fun create(): WebService {

            val client = OkHttpClient.Builder()
                .build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WebService::class.java)
        }
    }
}