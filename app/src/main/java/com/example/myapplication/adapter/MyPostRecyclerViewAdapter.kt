package com.example.myapplication.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.PostFragmentDirections
import com.example.myapplication.R
import com.example.myapplication.data.Post

class MyPostRecyclerViewAdapter(
//    private var clickListener: (String) -> Unit
) : RecyclerView.Adapter<MyPostRecyclerViewAdapter.ViewHolder>() {

    private var posts: MutableList<Post> = mutableListOf()

    fun setItems(postList: Collection<Post>) {
        posts.clear()
        posts.addAll(postList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_post_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = posts[position]
        holder.titleView.text = item.title
        holder.authorView.text = item.name

        holder.itemView.setOnClickListener {
            val direction = PostFragmentDirections
                .actionPostFragmentToPostDetailFragment(item.id)
            it.findNavController().navigate(direction)
        }
    }

    override fun getItemCount(): Int = posts.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val titleView: TextView = view.findViewById(R.id.title)
        val authorView: TextView = view.findViewById(R.id.author)

        override fun toString(): String {
            return super.toString() + " '" + titleView.text + " '" + authorView.text + "'"
        }
    }
}