package com.example.myapplication.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.myapplication.R
import com.example.myapplication.data.Comment
import com.example.myapplication.data.Post


class MyCommentRecyclerViewAdapter: RecyclerView.Adapter<MyCommentRecyclerViewAdapter.ViewHolder>() {

    private var comments: MutableList<Comment> = mutableListOf()

    fun setItems(postList: Collection<Comment>) {
        comments.clear()
        comments.addAll(postList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_comment_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = comments[position]
        holder.author.text = item.email
        holder.body.text = item.body
    }

    override fun getItemCount(): Int = comments.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val author: TextView = view.findViewById(R.id.comment_author_email)
        val body: TextView = view.findViewById(R.id.comment_body)

        override fun toString(): String {
            return super.toString() + " '" + body.text + "'"
        }
    }
}