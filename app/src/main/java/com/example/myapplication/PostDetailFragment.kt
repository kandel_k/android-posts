package com.example.myapplication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.navArgs
import com.example.myapplication.utils.InjectorUtils
import com.example.myapplication.viewModel.PostDetailViewModel
import kotlinx.android.synthetic.main.post_detail_fragment.*

class PostDetailFragment : Fragment() {

    companion object {
        fun newInstance() = PostDetailFragment()
    }

    private val args: PostDetailFragmentArgs by navArgs()

    private val viewModel: PostDetailViewModel by viewModels {
        InjectorUtils.providePostDetailViewModelFactory(requireContext(), args.postId)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.post_detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.postWithDetails.observe(viewLifecycleOwner) {
            if (it != null) {
                post_detail_title.text = it.post.title
                post_detail_body.text = it.post.body
                post_detail_author_name.text = it.user[0].name
                post_detail_author_email.text = it.user[0].email
            }
        }
    }

}